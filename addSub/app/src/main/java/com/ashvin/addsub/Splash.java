package com.ashvin.addsub;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;

/**
 * Created by Ashvin on 12/25/2014.
 */
public class Splash extends Activity {
    MediaPlayer ourSong;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        ourSong =  MediaPlayer.create(Splash.this, R.raw.as);
        ourSong.start();
        Thread timer = new Thread() {
            @Override
            public void run() {
                try {

                    sleep(5000);


                } catch (InterruptedException e) {
                    e.printStackTrace();

                } finally {
                    Intent openStartingPoint =  new Intent("android.intent.action.MAIN");
                    startActivity(openStartingPoint);
                }
                //super.run();
            }
        };
        timer.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        ourSong.release();
        finish();
    }
}
